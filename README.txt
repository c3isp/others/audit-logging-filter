Copyright 2018 Hewlett Packard Enterprise Development Company, L.P.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Simple servlet filter to log every request/response.


Eclipse
=======
You have to create a web.xml file under WebContent to run within eclipse for testing:

<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns="http://java.sun.com/xml/ns/javaee"
    xsi:schemaLocation="http://java.sun.com/xml/ns/javaee
                                http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
    id="WebApp_ID" version="3.0">
 
<display-name>Autologger</display-name>
 
<filter>
  <filter-name>auditLoggerFilter</filter-name>
  <filter-class>com.hpe.c3isp.logger.filters.AuditLoggingFilter</filter-class>
</filter>
 
<filter-mapping>
  <filter-name>auditLoggerFilter</filter-name>
  <url-pattern>/*</url-pattern>
</filter-mapping>
 
 
 
<welcome-file-list>
  <welcome-file>index.html</welcome-file>
  <welcome-file>index.htm</welcome-file>
  <welcome-file>index.jsp</welcome-file>
  <welcome-file>default.html</welcome-file>
  <welcome-file>default.htm</welcome-file>
  <welcome-file>default.jsp</welcome-file>
</welcome-file-list>
</web-app>

~~~~
IMPORTANT
=========
To run in eclipse, you must do the following:

In Project Properties, Deployment Assembly, Add..., Java Build Path Entries, Maven Dependencies.

TOMCAT
======
To deploy in tomcat, add the following filter definition to web.xml
<!-- == C3ISP audit filter - mm, 20181005 == -->
<filter>
    <filter-name>auditLoggerFilter</filter-name>
    <filter-class>com.hpe.c3isp.logger.filters.AuditLoggingFilter</filter-class>
</filter>

<filter-mapping>
    <filter-name>auditLoggerFilter</filter-name>
    <url-pattern>/*</url-pattern>
</filter-mapping>

=====================
