/**
*  Copyright 2019 Hewlett Packard Enterprise Development Company, L.P.
*/
package com.hpe.c3isp.logger.filters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.graylog2.gelfclient.GelfTransports;
import org.pmw.tinylog.Configurator;

/*import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.slf4j.MDC;*/

import org.pmw.tinylog.Logger;
import org.pmw.tinylog.LoggingContext;
import org.pmw.tinylog.writers.LogEntryValue;

import com.github.joschi.tinylog.gelf.GelfWriter;
import com.lespea.cef.CEF;
import com.lespea.cef.Extension;
import com.lespea.cef.InvalidExtensionKey;
import com.lespea.cef.InvalidField;

public class AuditLoggingFilter implements Filter {

/*    private static final Logger LOGGER = LoggerFactory
            .getLogger(AuditLoggingFilter.class);
    
    private static final Logger AUDIT = LoggerFactory
            .getLogger("AuditLog");*/
    
    private static final String DEVICE_VERSION = "1.0.0";
    private static final String DEVICE_VENDOR = "C3ISP";
    
    //private static final String APPLICATION_JSON_REGEX = ".*json.*";
    private static final String APPLICATION_REGEX = ".*application.*";
    private static final String SKIP_PATH_REGEX =
            ".*vaadinServlet.*|.*VAADIN.*|.*manager\\/text.*|.*manager\\/html.*|.*DSAEditor.*|.*DSAAuthoringTool.*|.*\\/api-docs.*|.*configuration\\/ui.*|.*configuration\\/security.*|.*swagger.*|.*c3isp-audit-manager.*|.*\\/favicon.ico";
    
    private static final String QUERY_SEP = "?";
    private static final String COMMA_SEP = ",";
    private static final String STATIC_VALUE_KV_SEP = ":";
    
    private static final String OUTCOME_SUCCESS = "Success";
    private static final String OUTCOME_FAILURE = "Failure";
    
    private static final int CEF_SEVERITY_INFO  =  5;
    //private static final int CEF_SEVERITY_WARN  =  8;
    private static final int CEF_SEVERITY_ERROR =  9;
    //private static final int CEF_SEVERITY_FATAL = 10;
    
    private FilterConfig filterConfig = null;
    
    // YXV0b2xvZ2dlcg== is base64("autologger")
    private static final String JVM_SPRING_PROFILE = "-Dspring.profiles.active=(.+)";
    private static final String ENV_SPRING_PROFILE = "SPRING_PROFILES_ACTIVE";
    private static final String BOOTSTRAP_FILE = "/bootstrap-YXV0b2xvZ2dlcg==.properties";
    private static final String DEFAULT_APP_PROPERTIES_FILE = "/application-YXV0b2xvZ2dlcg==.properties";
    
    enum ProfileFrom {
        BOOTSTRAP_FILE,
        ENV_VARIABLE,
        JVM_PROPERTY,
        NOT_SET
    }
    
    // correlation Id used in the MDC log
    final String corrIdLabel = "corrId";
    
    static {
        Logger.info("Loading autologger configuration: DEVICE_VENDOR="+DEVICE_VENDOR+", DEVICE_VERSION="+DEVICE_VERSION);
        
        Properties bootConfig = new Properties();
        try {
            bootConfig.load(AuditLoggingFilter.class.getResourceAsStream(BOOTSTRAP_FILE));
        } catch (Exception e) {
            e.printStackTrace();
            Logger.error("Cannot load bootstrap configuration: "+BOOTSTRAP_FILE);
        }
        
        Properties defaultConfig = new Properties();
        try {
            defaultConfig.load(AuditLoggingFilter.class.getResourceAsStream(DEFAULT_APP_PROPERTIES_FILE));
        } catch (Exception e) {
            e.printStackTrace();
            Logger.error("Cannot load default properties file: "+DEFAULT_APP_PROPERTIES_FILE);
        }
        
        String appName       = bootConfig.getProperty("spring.application.name");
        String configUser    = bootConfig.getProperty("spring.cloud.config.username");
        String configPass    = bootConfig.getProperty("spring.cloud.config.password");
        String configUri     = bootConfig.getProperty("spring.cloud.config.uri") == null ? "http://localhost:8888/" : bootConfig.getProperty("spring.cloud.config.uri");
        String configProfile = bootConfig.getProperty("spring.profiles.active");
        
        Logger.info("Config Server: appName=" + appName + ", configUser=" + configUser + ", configUri=" + configUri);
        
        String profile = "";
        ProfileFrom profileFrom = ProfileFrom.NOT_SET;
        
        // Extract profile from bootstrap.properties
        if (configProfile != null) {
            profile = configProfile;
            profileFrom = ProfileFrom.BOOTSTRAP_FILE;
        }
        // Extract profile from environment, if defined (ENV param overrides value in bootstrap.properties)
        if (System.getenv(ENV_SPRING_PROFILE) != null) {
            profile = System.getenv(ENV_SPRING_PROFILE);
            profileFrom = ProfileFrom.ENV_VARIABLE;
        }
        
        // Retrieve JVM parameters to see if there is the profile
        RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
        List<String> arguments = runtimeMxBean.getInputArguments();
        // Extract profile if defined in JVM param or in bootstrap.properties (JVM param overrides both ENV param and value in bootstrap.properties)
        String arg = arguments.stream()
            .filter(s->s.matches(JVM_SPRING_PROFILE))
            .findAny()
            .orElse("");
        Matcher m = Pattern.compile(JVM_SPRING_PROFILE).matcher(arg);
        if (m.matches()) {
            profile = m.group(1);
            profileFrom = ProfileFrom.JVM_PROPERTY;
        }
        
        Logger.info("Profile in use: " + profile + " - " + profileFrom.toString());
        
        // retrieve config properties
        Map<String, String> config = new HashMap<>();
        
        try {
            // Quick fix to avoid putting // in the URL path - needed by Spring Security
            if (!configUri.endsWith("/")) {
                configUri += "/";
            }
            URL url = new URL(configUri+appName+"/"+profile);
            Logger.info("Connecting to url="+url.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            String credentials = Base64.getEncoder().encodeToString(new String(configUser+":"+configPass).getBytes());
            conn.setRequestMethod("GET");
            conn.setDoOutput(true);
            conn.setRequestProperty("Authorization", "Basic " + credentials);
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                Logger.warn("Failed to use Config Server: HTTP error code=" + conn.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                String jsonConf = "";
                String line = "";
                while ((line = br.readLine()) != null) {
                    jsonConf += line;
                }
                conn.disconnect();
                Logger.info("Loaded configuration: " + jsonConf);
                
                /*{
                "name": "autologger",
                "profiles": [
                  "testbed"
                ],
                "label": null,
                "version": "36a985533b4609b0ff534960e62a8125ed923977",
                "state": null,
                "propertySources": [
                  {
                    "name": "https://devC3ISP.iit.cnr.it:8443/c3isp-wp7/config-server/testbed-config-repo.git/css/autologger.properties",
                    "source": {
                      "gelf.server": "auditmgrc3isp.iit.cnr.it",
                      "gelf.port": "12201",
                      "gelf.transport": "TCP",
                      "gelf.hostname": "",
                      "gelf.additionalLogEntryValues": "EXCEPTION,FILE,LINE",
                      "gelf.staticFields": ""
                    }
                  }
                ]
                }*/
                // process loaded configuration by using internal javascript engine (w/o any json lib)
                ScriptEngineManager scm = new ScriptEngineManager();
                ScriptEngine e = scm.getEngineByName("nashorn");
                Object obj = e.eval(
                        "Java.asJSONCompatible("+jsonConf+")"
                        );
                Map<String, Object> map1 = (Map<String, Object>)obj;
                List<Object> array = (List<Object>)map1.get("propertySources");
                Map<String, Object> map2 = (Map<String, Object>)array.get(0);
                config = (Map<String, String>)map2.get("source");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Logger.warn("Cannot use Config Server");
            // use default config file application.properties
            Logger.warn("Loading default configuration from: " + DEFAULT_APP_PROPERTIES_FILE);
            config = defaultConfig.entrySet().stream().collect(
                    Collectors.toMap(
                            item -> item.getKey().toString(),
                            item -> item.getValue().toString()
                       )
                   );
        }
        
        // Logs loaded configuration
        config.forEach((a, b) -> Logger.info("[" + a + "] = " + b));
        
        // Setup GelfWriter configuration by using the config Map
        
        // Build additionalLogEntryValues setting as a collection of LogEntryValue enums
        Collection<LogEntryValue> additionalLogEntryValues = new ArrayList<>();
        if (config.get("gelf.additionalLogEntryValues") != null) {
            Arrays.asList(config.get("gelf.additionalLogEntryValues").split(",")).forEach(item->{
                additionalLogEntryValues.add(LogEntryValue.valueOf(item));
            }); //additionalLogEntryValues.forEach(System.out::println);
        } else {
            additionalLogEntryValues.add(LogEntryValue.RENDERED_LOG_ENTRY); //set default if not specified
        }
        
        // Build staticFields
        Map<String, Object> staticFields; //= new HashMap<>();
        if (config.get("gelf.staticFields") != null && !("".equals(config.get("gelf.staticFields")))) {
            staticFields = new HashMap<>();
            Arrays.asList(config.get("gelf.staticFields").split(COMMA_SEP)).forEach(item->{
                int sep = item.indexOf(STATIC_VALUE_KV_SEP);
                if (sep != -1) {
                    String k = item.substring(0, sep);
                    String v = item.substring(sep + STATIC_VALUE_KV_SEP.length());
                    staticFields.put(k, v);
                } else {
                    Logger.warn("Skipping ["+item+"] from gelf.staticFields");
                }
            }); //staticFields.forEach((k,v)->System.out.println(k+","+v));
        } else {
            staticFields = Collections.<String, Object>emptyMap();
        }
        
        GelfWriter gelfWriter = new GelfWriter(
                config.get("gelf.server"),
                Integer.valueOf(config.get("gelf.port")),
                GelfTransports.valueOf(config.get("gelf.transport")),
                config.get("gelf.hostname"), //if null send current hostname
                EnumSet.copyOf(additionalLogEntryValues),
                staticFields);
        
        // Activate GELF logging if specified
        if (Boolean.valueOf(config.get("gelf.status"))) { 
            Logger.info("Logging to GELF server ENABLED by config");
            Configurator
                .defaultConfig()
                .addWriter(gelfWriter)
                .activate();
        } else {
            Logger.warn("Logging to GELF server DISABLED by config");
            Configurator
                .defaultConfig()
                .activate();
        }
    }
    
    public AuditLoggingFilter() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void destroy() {
        Logger.warn("AuditLoggingFilter stopped");
        this.filterConfig = null;
    }

    @SuppressWarnings("serial")
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        
        if (filterConfig == null)
            return;
        
        final HttpServletRequest  hreq = (HttpServletRequest)  request;
        final HttpServletResponse hres = (HttpServletResponse) response;
        
        String corrId = "";
        
        // simple criteria based on found headers for logging
        boolean loggingNeeded = false;
        /*Logger.warn("getContextPath="+hreq.getContextPath());
        Logger.warn("getServletPath="+hreq.getServletPath());
        Logger.warn("getServletContext.getContextPath="+hreq.getServletContext().getContextPath());
        Logger.warn("getPathTransalted="+hreq.getPathTranslated());*/
        if(
           (!hreq.getServletPath().matches(SKIP_PATH_REGEX) && !hreq.getContextPath().matches(SKIP_PATH_REGEX)) &&
           (
                   (hreq.getHeader("Accept") != null && hreq.getHeader("Accept").matches(APPLICATION_REGEX))
                   ||
                   (hreq.getContentType() != null && hreq.getContentType().matches(APPLICATION_REGEX))
                   ||
                   (hres.getHeader("Accept") != null && hres.getHeader("Accept").matches(APPLICATION_REGEX))
                   ||
                   (hres.getContentType() != null && hres.getContentType().matches(APPLICATION_REGEX))
            )
          ) {
            Logger.debug("Found REST call based on content type or accept headers");
            loggingNeeded = true;
            hres.setHeader("X-C3ISP-logger", "application-autologger");
        }
        
        //hres.setHeader("X-C3ISP-test", "TEST!!");

        if (loggingNeeded) {
            /*if (MDC.get("counter")!=null) {
                Integer c = Integer.parseInt(MDC.get("counter"));
                MDC.put("counter", String.valueOf(++c));
                LOGGER.info("counter before="+MDC.get("counter")+"c="+c);
            } else {
                MDC.put("counter", String.valueOf(1));
                LOGGER.info("Init counter="+MDC.get("counter"));
                // Set an ID useful to correlate all the requests that might spawn
                corrId = UUID.randomUUID().toString();
                MDC.put(corrIdLabel, corrId);
            }*/
            // Set an ID useful to correlate the request and response
            corrId = UUID.randomUUID().toString();
            LoggingContext.put(corrIdLabel, corrId);
            Logger.debug("Logging request");
            try {
                final String suser = hreq.getUserPrincipal() != null ? hreq.getUserPrincipal().getName() : (hreq.getRemoteUser() != null ? hreq.getRemoteUser() : ""); // principle name or remote user (if available)
                //AUDIT.info(new CEF("C3ISP", "/springswagger-template/", "1.0.0", "POST", "POST on /springswagger-template/v1/connect", 6,
                Logger.info(new CEF(DEVICE_VENDOR, hreq.getContextPath(), DEVICE_VERSION, hreq.getMethod(), "Request to "+hreq.getMethod()+" on "+hreq.getRequestURI(), CEF_SEVERITY_INFO,
                        new Extension(new HashMap<String, String>() {{
                            put("app", hreq.getProtocol()); // e.g. HTTP/1.1
                            put("deviceDirection", "0"); // "0" for inbound or "1" for outbound
                            put("dhost", hreq.getLocalName()); // destination hostname
                            put("dst", hreq.getLocalAddr()); // destination IP
                            put("shost", hreq.getRemoteHost()); // source hostname
                            put("src", hreq.getRemoteAddr()); // source IP
                            put("request", String.valueOf(hreq.getRequestURL().append(hreq.getQueryString() != null ? QUERY_SEP+hreq.getQueryString():""))); // requested URL
                            put("requestMethod", hreq.getMethod()); // http method
                            if (!"".equals(suser)) put("suser", suser);
                            put("msg", hreq.getMethod()+" on "+hreq.getRequestURI()); // a message like "POST on /v1/connect"
                        }} )).toString());
            } catch (InvalidField e) {
                Logger.warn("Invalid CEF Field");
                e.printStackTrace();
            } catch (InvalidExtensionKey e) {
                Logger.warn("Invalid CEF Extension Key");
                e.printStackTrace();
            }

        } else
            Logger.debug("Non-REST call received");
        
        chain.doFilter(request, response);
        
        if (loggingNeeded) {
            Logger.debug("Logging response");
            
            try {
                final String outcome = (String.valueOf(hres.getStatus()).startsWith("2") ? OUTCOME_SUCCESS : OUTCOME_FAILURE);
                final String suser = hreq.getUserPrincipal() != null ? hreq.getUserPrincipal().getName() : (hreq.getRemoteUser() != null ? hreq.getRemoteUser() : ""); // principle name or remote user (if available)
                //AUDIT.info(new CEF("C3ISP", "/springswagger-template/", "1.0.0", "POST", "POST on /springswagger-template/v1/connect", 6,
                Logger.info(new CEF("C3ISP", hreq.getContextPath(), "1.0.0", hreq.getMethod(),
                        "Response to "+hreq.getMethod()+" on "+hreq.getRequestURI(),
                        outcome == OUTCOME_SUCCESS ? CEF_SEVERITY_INFO : CEF_SEVERITY_ERROR,
                        new Extension(new HashMap<String, String>() {{
                            put("app", hreq.getProtocol()); // e.g. HTTP/1.1
                            put("deviceDirection", "1"); // "0" for inbound or "1" for outbound
                            put("dhost", hreq.getLocalName()); // destination hostname
                            put("dst", hreq.getLocalAddr()); // destination IP
                            put("outcome", outcome); // success=http/200, failure=others
                            put("reason", String.valueOf(hres.getStatus())); // http status code
                            put("shost", hreq.getRemoteHost()); // source hostname
                            put("src", hreq.getRemoteAddr()); // source IP
                            put("request", String.valueOf(hreq.getRequestURL().append(hreq.getQueryString() != null ? QUERY_SEP+hreq.getQueryString():""))); // requested URL
                            put("requestMethod", hreq.getMethod()); // http method
                            if (!"".equals(suser)) put("suser", suser);
                            put("msg", hreq.getMethod()+" on "+hreq.getRequestURI()); // a message like "POST on /v1/connect"
                        }} )).toString());
                
                /*if (MDC.get("counter")!=null) {
                    Integer c = Integer.parseInt(MDC.get("counter"));
                    MDC.put("counter", String.valueOf(--c));
                    LOGGER.info("counter after="+MDC.get("counter")+"c="+c);
                    // remove current corrId
                    if (c < 0) {
                        LOGGER.info("remove counter c={} and label", c);
                        MDC.remove(corrIdLabel);
                        MDC.remove("counter");
                    }
                }*/
                // remove current corrId
                LoggingContext.remove(corrIdLabel);
            
            } catch (InvalidField e) {
                Logger.warn("Invalid CEF Field");
                e.printStackTrace();
            } catch (InvalidExtensionKey e) {
                Logger.warn("Invalid CEF Extension Key");
                e.printStackTrace();
            }
            
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Logger.info("Startup AuditLoggingFilter");
        this.filterConfig = filterConfig;
    }

}
